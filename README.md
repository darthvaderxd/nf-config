## nf-config-vault
# NilFactor Config Server with vault support

This is a simple to use and setup configuration server. Easy to use and configure.
This allows you to interface with vault and get secrets out of there.

## Getting Started

Edit your <NODE_ENV>.json (or development.json by default).

```
{
    "nfMultiToken": {
        "apiKey": "token",
        "apiUsers": [
            {
                "apiUser": "root",
                "apiToken": "deadbeef",
                "routes": ["*"]
            },
            {
                "apiUser": "read_only",
                "apiToken": "1337beef",
                "routes": [
                    "/list",
                    "/config/get/*"
                ]
            }
        ]
    },
    "vaultConfig": {
        "apiVersion": "v1",
        "endpoint": "http://127.0.0.1:8200"
    }
}
```

|Name|Description|
| -------|-----------------------------------------------------------------------------------------------------|
|apiKey|This is the key name of the header object we are looking to verify for proper authenticate (lower case)|
|apiUsers|This is and array which allows you to assign access based on token given|
|apiUser|the name of the user we are configuring|
|apiToken|the token they will use|
|routes|an array of routes they have access to, you could narrow down users to specific config types with this|

## Enabling HTTPS

Edit the index.js to look like the following

```
'use strict';

const server = require('./lib/server');
const fs = require('fs');

var options = {
    key: fs.readFileSync('/path/to/server.key'),
    certificate: fs.readFileSync('/path/to/server.crt')
};

server.run(options);
```

## API Routes

|URL|Method|Description|
|-------------|------|----------------------------------------------------------------------------|
|/list|GET|returns the list of configurations saved into the service|
|/config/get/:name|GET|returns the stored configuration requested|
|/config/update/:name|POST|allows you to submit json string in the body to create/update a configuration|
|/config/update/:name|DELETE|deletes the stored configuration|

## Configs
Instead of posting your configs you could simply create named json files in the configs directory. Then to retrieve them
you would simply do request the name of the configuration minus .json. So if your file was called 'production.json',
your url would be http://localhost:[port]/config/production

example with vault support

```
{
    "name": "production",
    "db": {
        "getVaultSecret": "db"
    },
    "shards": [
        {"getVaultSecret": "db2"},
        {"getVaultSecret": "db3"}
    ],
    "live": true
}
```

or your whole config could be stored in vault

```
{
    "getVaultSecret": "dev"
}
```

## Setting up testing environment

```
docker-compose up vault
```

First you would need to unlock the vault

```
node init_vault.js
```

Then take toke root_token given and do

```
export VAULT_TOKEN=my_vault_token
```