var vault = require('./lib/vault');

var load = async () => {
    console.log('setting up data for development');

    await vault.setSecret('helloWorld', 'hello_world');
    await vault.setSecret('db', {
        host: 'some_host_name',
        port: 5432,
        password: 'bob',
        dbName: 'edb'
    });

    await vault.setSecret('db2', {
        host: 'some_host_name_2',
        port: 5432,
        password: 'bobbo',
        dbName: 'edb'
    });

    await vault.setSecret('db3', {
        host: 'some_host_name_3',
        port: 5432,
        password: 'bobba',
        dbName: 'edb'
    });
}

load();