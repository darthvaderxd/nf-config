'use strict';

/* istanbul ignore next */
const port = process.env.PORT || 8080;
const handler = require('./handler');

var server;

module.exports.run = (options, callback) => {
    server = require('./server')(options);
    handler.load();

    server.listen(port, () => {
        /* istanbul ignore if */
        if (!process.env.SILENT) {
            console.log('%s listening at %s', server.name, server.url);
        }

        /* istanbul ignore if */
        if (typeof callback != 'undefined') {
            callback();
        }
    });

    server.get('/list', handler.list);
    server.get('/config/get/:name', handler.getConfig);
    server.post('/config/update/:name', handler.saveConfig);
    server.del('/config/update/:name', handler.deleteConfig);
};


module.exports.stop = () => {
    server.close();
};