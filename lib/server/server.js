'use strict';

const restify = require('restify');
const token = require('nf-multi-token');

var configureParams = (options) => {
    /* istanbul ignore next */
    if (typeof options === 'undefined') {
        options = {};
    }

    options.name = 'nf-config';

    return options;
};

module.exports = (options) => {
    var params = configureParams(options);
    var server = restify.createServer(params);

    server.use(restify.plugins.acceptParser(server.acceptable));
    server.use(restify.plugins.queryParser());
    server.use(restify.plugins.bodyParser());

    server.pre(token.verifyRequest);
    return server;
};