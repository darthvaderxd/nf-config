'use strict';

const config = require('../config');
const http = require('http-status');

module.exports.load = () => {
    config.load();
};

module.exports.list = (req, res, next) => {
    var configs = config.getConfigs(true);
    res.send({result: configs});
    return next();
};

module.exports.getConfig = (req, res, next) => {
    var name = req.params.name;
    var result = config.getConfig(name);
    var code = http.NOT_FOUND;
    var send = {
        config: name,
        result: false
    };

    if (result) {
        code = http.OK;
        send.result = result;
    }

    res.send(code, send);
    return next();
};

module.exports.saveConfig = (req, res, next) => {
    var name = req.params.name;
    var data = req.body;
    var code = http.INTERNAL_SERVER_ERROR;
    var send = {result: false};

    try {
        config.setConfig(name, data);
        send = {result: true};
        code = http.ACCEPTED;
    } catch(e) { /* istanbul ignore next */
        console.log(e);
        /* istanbul ignore next */
        send.error = e;
    }

    res.send(code, send);
    return next();
};

module.exports.deleteConfig = (req, res, next) => {
    var name = req.params.name;
    var code = http.INTERNAL_SERVER_ERROR;
    var send = {result: false};

    try {
        config.deleteConfig(name);
        send = {result: true};
        code = http.ACCEPTED;
    } catch(e) { /* istanbul ignore next */
        console.log(e);
        /* istanbul ignore next */
        send.error = e
    }

    res.send(code, send);
    return next();
};