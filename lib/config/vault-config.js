'use strict';

const vault = require("../vault");

/**
 * @brief loop over array and process
 * @param array configArray - array with more items in it needed to be looped through
 * @return array
 */
var handleArray = async (configArray) => {
    configArray.forEach(async (value, key) => {
        configArray[key] = await parseConfig(value);
    });

    return configArray;
};

/**
 * @brief determine of object is a request to get from vault if so get it if not walk through keys of object
 * @param array configObject - object with more items in it needed to be looped through
 * @return object
 */
var handleObject = async (configObject) => {
    if (typeof configObject.getVaultSecret === 'undefined') {
        configObject = await walkObject(configObject);
    } else {
        configObject = await getVaultSecret(configObject);
    }

    return configObject;
};

/**
 * @brief loop over keys in object
 * @param object configObject - object with more items in it needed to be looped through
 * @return object
 */
var walkObject = async (configObject) => {
    for (var key in configObject) {
        configObject[key] = await parseConfig(configObject[key]);
    }

    return configObject;
};

/**
 * @brief get the secret out of vault
 * @param object configSecret - object with vault secret
 * @return object/string
 */
var getVaultSecret = async (configSecret) => {
    var secret = await vault.getSecret(configSecret.getVaultSecret);
    try {
        configSecret = JSON.parse(secret);;
    } catch (error) {
        configSecret = secret;
    }

    return configSecret;
};

/**
 * @brief loop through the loaded config file, allowing us to load data from vault where needed
 * @param object config - object of config data
 * @return object
 */
var parseConfig = async (config) => {
    if (Array.isArray(config) && config.length > 0) {
        config = await handleArray(config);
    } else if (typeof config === 'object') {
        config = await handleObject(config);
    }

    return config;
};

module.exports.parseConfig = parseConfig;