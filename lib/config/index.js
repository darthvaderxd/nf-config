'use strict';

const fs = require('fs');
const folder = './configs/';
const vc = require('./vault-config');

var configs = {};

var getSaveFolder = () => {
    return folder;
};

var getConfig = (name) => {
    return configs[name] || false;
};

var getConfigs = (namesOnly) => {
    if (!namesOnly) {
        return configs;
    } else {
        return Object.getOwnPropertyNames(configs);
    }
};

var setConfig = (name, data) => {
    saveConfig(name, data);
    configs[name] = data;
};

var deleteConfig = (name) => {
    var filename = name + ".json";
    var filePath = folder + filename;
    if (fileExists(filePath)) {
        fs.unlink(filePath, (err) => {
            /* istanbul ignore if */
            if (err) {
                throw err;
            }

            /* istanbul ignore if */
            if (!process.env.SILENT) {
                console.log("deleted %s", name);
            }
        });
        delete(configs[name]);
    }
};

var load = () => {
    configs = {};
    fs.readdir(folder, (err, files) => {
        /* istanbul ignore if */
        if (err) {
            throw err;
        }

        if (files && files.length > 0) {
            files.forEach((filename) => {
                loadConfig(filename);
            });
        } else {
            /* istanbul ignore if */
            if (!process.env.SILENT) {
                console.log("there are no configs to load");
            }
        }
    });
};

var loadConfig = (filename) => {
    var filePath = folder + filename;
    if (fileExists(filePath)) {
        fs.readFile(filePath, 'utf-8', async (err, contents) => {
            /* istanbul ignore if */
            if (err) {
                throw err;
            }

            var json = JSON.parse(contents);
            var data = await vc.parseConfig(json);
            var parts = filename.split(".json");
            var name = parts[0];
            configs[name] = data;

            /* istanbul ignore if */
            if (!process.env.SILENT) {
                console.log('loaded %s', name);
            }
        });
    }
};

var saveConfig = (name, data) => {
    var filename = name + ".json";
    var filePath = folder + filename;
    var contents = JSON.stringify(data);
    fs.writeFile(filePath, contents, (err) => {
        /* istanbul ignore if */
        if (err) {
            throw err;
        }

        /* istanbul ignore if */
        if (!process.env.SILENT) {
            console.log("saved %s", name);
        }
    });
};

var fileExists = (filePath) => {
    try {
        fs.lstatSync(filePath);
        return true;
    } catch(e) {
        return false;
    }
};

module.exports.getSaveFolder = getSaveFolder;
module.exports.getConfig = getConfig;
module.exports.getConfigs = getConfigs;
module.exports.setConfig = setConfig;
module.exports.deleteConfig = deleteConfig;
module.exports.load = load;
module.exports.fileExists = fileExists;