'use strict';

const config = require('config-node')();
const vault = require("node-vault")(config.vaultConfig);

var doSecretAction = (action, secret, value) => {
    var uri = `secret/${secret}`;
    return new Promise((resolve, reject) => {
        vault[action](uri, value)
            .then((result) => {
                if (result && typeof result.data != 'undefined') {
                    resolve(result.data.value);
                } else {
                    resolve(result);
                }
            });
    });
};

module.exports.getSecret = (secret) => {
    return doSecretAction('read', secret);
};

module.exports.setSecret = (secret, value, lease) => {
    if (typeof value === 'object') {
        value = JSON.stringify(value);
    }

    value = {
        value: value,
        lease: lease || '1s'
    }

    return doSecretAction('write', secret, value);
};

module.exports.deleteSecret = (secret) => {
    return doSecretAction('delete', secret);
};