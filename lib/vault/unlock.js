'use strict';

const config = require('config-node')();
const vault = require("node-vault")(config.vaultConfig);

module.exports.init = () => {
    var params = {secret_shares: 1, secret_threshold: 1};
    return vault.init(params);
};

module.exports.unlock = (key) => {
    var params = {secret_shares: 1, key: key};
    return vault.unseal(params);
};