'use strict';

var vaultLock = require('./lib/vault/unlock');

var run = async () => {
    var token = await vaultLock.init();
    var vaultInfo = await vaultLock.unlock(token.keys[0]);


    console.log('VAULT_TOKEN => ', token);
    console.log('vault => ', vaultInfo);
    process.env.VAULT_TOKEN = token;
};

run();