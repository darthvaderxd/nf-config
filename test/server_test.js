'use strict';

process.env.PORT = 9123;
process.env.SILENT = true;

const assert = require('assert');
const request = require('request');
const server = require('../lib/server');
const baseUrl = 'http://localhost:9123';

const doRequest = (params) => {
    return new Promise((resolve, reject) => {
        request(params, (error, response, body) => {
            if (error) {
                reject(error);
            }
            body = JSON.parse(body);
            resolve(body);
        });
    });
};

describe('server library', () => {
    describe('base functionality', () => {
        it('listens and loads without error', () => {
            server.run();
        });

        it('responds to a request', () => {
            request(`${baseUrl}/list`, (error, response, body) => {
                assert(error === null);
            });
        });

        it('requires a token', () => {
            request(`${baseUrl}/list`, (error, response, body) => {
                body = JSON.parse(body);
                assert(body.code === 'UnauthorizedRequest');
            });
        });

        it('accepts token', () => {
            var params = {
                url: `${baseUrl}/list`,
                headers: {
                    'token': 'deadbeef'
                }
            };
            request(params, (error, response, body) => {
                body = JSON.parse(body);
                assert(typeof body.code === 'undefined');
            });
        });
    });

    describe('route functionality with data', () => {
        it('post /config/update/:name posts with no errors', async () => {
            var data = {
                'code': 'foobar'
            };
            var params = {
                url: `${baseUrl}/config/update/server_test_one`,
                headers: {
                    'token': 'deadbeef'
                },
                form: data,
                method: 'POST'
            };
            var result = await doRequest(params);

            assert(result.result === true);
        });

        it('get /config/:name gets config with no errors', async () => {
            var params = {
                url: `${baseUrl}/config/get/server_test_one`,
                headers: {
                    'token': 'deadbeef'
                }
            };
            var result = await doRequest(params);

            assert(result.config === 'server_test_one');
            assert(result.result.code === 'foobar');
        });

        it('get /list gets the list with no errors', async () => {
            var params = {
                url: `${baseUrl}/list`,
                headers: {
                    'token': 'deadbeef'
                }
            };
            var result = await doRequest(params);

            assert(result.result[0] === 'server_test_one');
        });

        it('delete /config/:name deletes with no errors', async () => {
            var params = {
                url: `${baseUrl}/config/update/server_test_one`,
                headers: {
                    'token': 'deadbeef'
                },
                method: 'DELETE'
            };
            var result = await doRequest(params);

            assert(result.result === true);
        });
    });

    describe('route functionality with no valid results or empty data', () => {
        it('get /config/:name gets config with no errors', async () => {
            var params = {
                url: `${baseUrl}/config/get/empty`,
                headers: {
                    'token': 'deadbeef'
                }
            };
            var result = await doRequest(params);

            assert(result.config === 'empty');
            assert(result.result === false);
        });

        it('get /list gets the list with no errors', async () => {
            var params = {
                url: `${baseUrl}/list`,
                headers: {
                    'token': 'deadbeef'
                }
            };
            var result = await doRequest(params);

            assert(result.result.length === 0);
        });

        it('delete /config/:name deletes with no errors', async () => {
            var params = {
                url: `${baseUrl}/config/update/empty`,
                headers: {
                    'token': 'deadbeef'
                },
                method: 'DELETE'
            };
            var result = await doRequest(params);

            assert(result.result === true);
        });
    });

    describe('closes without error', () => {
        it('stops', () => {
            server.stop();
        });
    })
});