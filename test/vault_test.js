var assert = require('assert');
var initVault = require('../lib/vault/unlock');
var vault = null;
var password = "this_is_my_secret";
var object = {hello: "world"};

var unlockKey = '';

describe('vault library', () => {
    it('gets new token without error', async () => {
        var result = await initVault.init();
        assert(typeof result === 'object');
        assert(typeof result.root_token === 'string');
        unlockKey = result.keys[0];

        process.env.VAULT_TOKEN = result.root_token;
    });

    it('unlocks without error', async () => {
        var result = await initVault.unlock(unlockKey);
        assert(typeof result === 'object');
        vault = require('../lib/vault');
    });

    it('sets a secret without error', async () => {
        var result = await vault.setSecret('pw', password);
        assert(typeof result === 'undefined');
    });

    it('gets a secret and matches expected without error', async () => {
        var pw = await vault.getSecret('pw');
        assert(pw == password);
    });

    it('sets a secret as an object without error', async () => {
        var result = await vault.setSecret('pw', object);
        assert(typeof result === 'undefined');
    });

    it('deletes secret without error', async () => {
        var resultOne = await vault.deleteSecret('pw');
        assert(typeof resultOne === 'undefined');
    });
});