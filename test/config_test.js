'use strict';

process.env.SILENT = true;

const assert = require('assert');
const config = require('../lib/config');
const folder = config.getSaveFolder();

describe('config library', () => {
    describe('setConfig(name, data)', () => {
        var name = "test_one";
        var data = {
            foo: "bar"
        };
        var configs = config.getConfigs(true);
        var startCount = configs.length || 0;
        var filename = name + ".json";
        var filePath = folder + filename;

        config.setConfig(name, data);

        configs = config.getConfigs(true);
        var endCount = configs.length || 0;

        it('saves the config to the expected location: ' + filePath, () => {
            assert(config.fileExists(filePath) === true);
        });

        it('added exactly one entry', () => {
            assert(startCount + 1 === endCount);
        });
    });

    describe('deleteConfig(name)', () => {
        it('deletes the file as expected and only removes one entry', () => {
            var name = "test_one";
            var filename = name + ".json";
            var filePath = folder + filename;

            setTimeout(() => {
                var configs = config.getConfigs(true);
                var startCount = configs.length || 0;

                config.deleteConfig(name);

                configs = config.getConfigs(true);
                var endCount = configs.length || 0;

                assert(config.fileExists(filePath) === false);
                assert(endCount === startCount - 1);
            }, 250);
        });

        it('does not crash or error when trying to delete file that does not exist', () => {
            config.deleteConfig("abcdefg1234587679817123987614359786");
        });
    });

    describe('getConfig(name)', () => {
        it('returns false if empty', () => {
            var result = config.getConfig('bob');
            assert(result === false);
        });

        it('returns expected named object', () => {
            var name = 'test_two';
            var data = {
                message: "helloWorld"
            };
            config.setConfig(name, data);

            var result = config.getConfig(name);
            assert(data === result);

            // cleanup
            config.deleteConfig(name);
        });
    });

    describe('load()', () => {
        it('it clears before loading, no double entries', () => {
            var name = 'test_three';
            var data = {
                message: "BOB"
            };
            config.setConfig(name, data);

            var configs = config.getConfigs();
            var startCount = configs.length;

            config.load();
            configs = config.getConfigs();
            var endCount = configs.length;

            assert(startCount === endCount);

            //cleanup
            config.deleteConfig(name);
        });

        it('does not error when loading no configs (empty)', () => {
            var configs = config.getConfigs(true);
            configs.forEach((name) => {
                config.deleteConfig(name);
            });
            config.load();
        });
    });
});