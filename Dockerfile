FROM node:9.8-alpine
WORKDIR /app
COPY package.json /app
RUN npm install --no-optional
COPY . /app
RUN mkdir -p /app/configs/
VOLUME /app/configs

EXPOSE 8080

CMD ["node", "index.js"]
